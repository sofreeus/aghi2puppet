#!/bin/cat

A Gentle Hands-on Introduction to Puppet
========================================

Chapter 0 - Introductions and Setup
-----------------------------------

To do
-----
 * Hand out USB sticks with w1,s1,s2, at least
 * Students choose partners
 * Pair lab:
   - Import Machines
   - git clone https://gitlab.com/sofreeus/aghi2puppet.git
   - git clone git@gitlab.com:sofreeus/aghi2puppet.git
 * Teacher introduces SFS, teachers and staff, host org

Participant Introductions Ideas:
--------------------------------
 * name
 * experience and expectations
 * something we all ought to know
 * something you always have to do
   (This is a job for Puppet!)

Ways to contribute:
-------------------
 * Specific feedback, praise, and suggestions.
 * Improve the [code and documents](https://gitlab.com/sofreeus/aghi2puppet).
 * Come to Linux Camp or help sponsor someone.

Chapter 1 - What is Puppet
--------------------------

What is Puppet?

Puppet is a domain-specific programming language for system-management.

What sorts of "things" do sysadmins do to the systems they manage? What sorts of things do you do over-and-over?

Let participant's build a list. It might include installing software, setting up users, copying in and out files, scheduling backups, resetting passwords, configuring services...

Demo:

One thing I do over-and-over (that I can't do in Puppet) is ... install Puppet.

```
cd ~/git/aghi2puppet/
./scripts/install_puppet
```

Pair Lab: On all systems, try these

```
puppet resource user root
puppet resource user (you)
puppet help resource
puppet resource --help user
facter | less
facter hostname
```

Chapter 2 - Core Resources
--------------------------

In Puppet, the basic unit of management is the resource. A resource is of a particular type.

Most sysadmin tasks are built around one of only several core resource types, like: package, file, service, user, group, notify, exec, and cron

cf: [Puppet Core Types Cheetsheet](http://docs.puppetlabs.com/puppet_core_types_cheatsheet.pdf)

Demo:

 - review and run add_hosts.pp and dlwillson_admin.pp
 - use puppet resource host x and puppet resource user y to rough in commands
 - use puppet parser validate before commits

Pair Lab:

 * copy dlwillson_admin.pp to username_admin.pp
 * update username_admin.pp with:
   - your username
   - your public key
   - your password hash
 * puppet parser validate username_admin.pp
 * puppet apply username_admin.pp
 * modify and apply addhosts.pp
 * copy addhosts.pp to the other system and apply it
 * copy username_admin.pp to other system and apply it

Chapter 3 - The Trifecta
------------------------

One very common pattern in Puppet programs is the trifecta: package, file, service

*Important*: Puppet programs are not procedural. Resources may be realized in any order. To be sure that resources are checked in a useful order, use 'require', 'notify', and/or 'subscribe' appropriately.

Demo: Run simplenfs.pp and simplesamba.pp to create a simple NAS.

Pair Lab: Do the same.

Chapter 4 - Modules
-------------------

 * Modules Lecture

   - What is a module?
   - How to convert a program into a module
   - files: source, content, and templates

 * Modules Demo: Convert the simplesamba program to a module and run it.

 * Lab 3 - Convert all programs created thus far into modules

 * If time allows or as homework: Build Fedora and Ubuntu and modify the network so all machines see each other.

Chapter 5 - External Modules
----------------------------

 * Getting things from puppet-forge, github, etc...
 * Lab - implement saz-sudo or puppetlabs-apache

Chapter 6 - Puppet Master and Agents
------------------------------------

 * Puppet Master and Agents
 * Demo/Lab:

  - Copy modules to /etc/puppet/modules (or symlink or git-repo)

 * Demo/Lab:

  - Install Puppet on Fedora and Ubuntu
  - Configure agents to talk to Puppet Master
  - Sign certificates

 * Discuss site manifests

  - Demo/Lab: Modify site manifest

 * Discuss roles

  - Samba + NFS might be a "NAS" role

Chapter 7 - Things we can talk about but probably won't get to try.
---------------------------------------

   - Environments
   - Parameterized classes
   - Content templates
   - Defined resource types
   - Craig Dunn's Roles and Profiles pattern
