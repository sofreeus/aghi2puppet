License:

All images, and this repository, are licensed under CC BY-SA 4.0, unless otherwise noted or are already specifically copyrighted by it's original creator.

https://creativecommons.org/licenses/by-sa/4.0/
