node s2 {
  include dlwillson_admin,simplesamba,simplenfs
}

node default {
  notify { 'default':
    message => "Hello host '${::hostname}' from the '${::environment}' environment. You have hit the 'default' node definition",
  }
}

