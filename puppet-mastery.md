How to configure a Puppet Master and Agents
===

https://docs.puppet.com/puppet/3.8/reference/post_install.html#configure-a-puppet-master-server

---

On the puppet-master:

add dns_alt_names to [main] in /etc/puppet/puppet.conf

`dns_alt_names = puppet`

add puppet alias to /etc/hosts

`10.0.2.34	a2p-somename puppet`


Install and start the Puppet Master role
---

```shell
sudo yum install puppet-server
sudo systemctl enable puppetmaster
sudo systemctl start puppetmaster
sudo systemctl status puppetmaster
sudo puppet agent --test
# build /etc/puppet/manifests/site.pp from aghi2puppet/manifests/site.pp
sudo puppet agent --test
# add a host definition to /etc/puppet/manifests/site.pp
sudo puppet agent --test
```


Configure an Agent to talk to the Master
---

master:

```shell
sudo systemctl stop firewalld
sudo systemctl disable firewalld
```

agent:

```shell
sudo puppet agent --test --verbose
```

master:

```shell
sudo puppet cert list
sudo puppet cert sign "somename"
```

agent:

```shell
sudo puppet agent --test --verbose
```

On the Master, add a host definition for the Agent.
master: `sudo vim /etc/puppet/manifests/site.pp`
puppet: `sudo puppet agent --test --verbose`

---

Copy modules simplesamba and you_admin to /etc/puppet/modules/
'include' the modules in machine definitions
run puppet agent on machines
confirm desired results
