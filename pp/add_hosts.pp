host { 'a2p-workstation':
  ensure       => 'present',
  ip           => '10.0.2.34',
}
host { 'a2p-server':
  ensure       => 'present',
  ip           => '10.0.2.35',
}
