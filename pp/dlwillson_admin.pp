user { 'dlwillson':
  ensure     => present,
  password   => '$6$kYq44Y.V$V6GYnaUPUodu4QaHEXmMyNnTZ.xbi2q0Y9wXduR1TKVKgn4hpGVGS/vV/RRSqAiJG2n8vM3I3Bpy9iOa0ruVT0',
  managehome => true,
  shell      => '/bin/bash',
}

ssh_authorized_key { 'dlwillson@a2p-workstation':
  ensure => 'present',
  key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCiupAbgOkX8DhKsFrhAOsVftVL9jtyMAEaLj5zwDtofvFUPjoPlUTOuI52pQJD5YmGvc+/3sfXUZWBio171qnmkgiVsbhyvdtzPcnMdUrl8fW+S0G5BUmL0NKAzTnX0ZJjMD/07u8+Rh2eczzAtn4W0ujYxM4CWp+R4DvXZdpC6xRnmGLzGr7DXaAx0HGjN8xQo1NEuhdtv1JqmYWYNTeJe0pmUtSDX9TeYhEW+S2AnrvUSKoM3SRTfnM+jZ2tu21A9Z/4DGPylgAUe1RQWTfcXNWf+EnjZJ+GIM8ubF8VbUBkVhVbik6CXzNcGML1ygacX2Pu2fgQR8YNn9bo0E+d',
  type   => 'ssh-rsa',
  user   => 'dlwillson',
}

file { '/etc/sudoers.d/dlwillson':
  ensure  => 'present',
  mode    => '0440',
  owner   => 'root',
  group   => 'root',
  content => 'dlwillson ALL=(ALL) NOPASSWD:ALL',
}
