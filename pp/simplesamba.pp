  package {'samba':
    ensure => present,
  }
  file {'/etc/samba/smb.conf':
    ensure  => present,
    source  => 'file:///tmp/smb.conf',
    require => Package['samba'],
    notify  => Service['smb'],
  }
  file {'/simplesambashare':
    ensure => directory,
    mode   => '1777',
  }
  service  {'smb':
    ensure => running,
    enable => true,
  }
