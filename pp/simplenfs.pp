file { '/etc/exports':
  ensure => 'file',
  content => '/shared *(no_root_squash,rw)',
  notify => Service['nfs'],
  require => Package['nfs'],
}

package { 'nfs':
  ensure => 'installed',
}

service { 'nfs':
  ensure => 'running',
  enable => true,
}
