
package { 'postgresql-server':
  ensure => 'latest',
  notify => Service['postgresql'],
}

service { 'postgresql':
  ensure => 'running',
  enable => 'true',
}

file { '/etc/postgresql.conf':
  ensure => 'file',
  group  => '0',
  mode   => '644',
  owner  => '0',
  source => 'file:///usr/share/pgsql/postgresql.conf.sample',
  notify => Service['postgresql'],
}

