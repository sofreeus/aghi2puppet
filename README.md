AGHI2 Puppet
============

A Gentle Hands-on Introduction to Puppet

Facilitators should be intimately familiar with Puppet, at least resources, modules, CLI, PuppetForge and other external modules, some strategy for role-management. Either of the Datalogix approach or the Craig Dunn approach is fine. 

Participants may be of almost any experience level, but those with Linux sysadmin and programming fundamentals firmly in place will enjoy the class best.

This repository should hold all the small files needed to teach the one-day class: A few Puppet programs, a few modules, a slideshow, and some lab definitions.
