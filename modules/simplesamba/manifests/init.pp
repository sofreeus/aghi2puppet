class simplesamba {
  package {'samba':
    ensure => present,
  }
  file {'/etc/samba/smb.conf':
    ensure  => present,
    path    => '/etc/samba/smb.conf',
    source  => 'puppet:///modules/simplesamba/smb.conf',
    require => Package['samba'],
    notify  => Service['smb'],
  }
  file {'/simplesambashare':
    ensure => directory,
    mode   => '1777',
  }
  service  {'smb':
    ensure => running,
    enable => true,
  }
}
