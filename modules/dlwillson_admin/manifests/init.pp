# Second Exercise - Turn it into a module and include the module from a node definition.

class dlwillson_admin {

user { 'dlwillson':
  ensure     => present,
  password   => '$6$vB4ds8i0$S99TXOJlF9Fq3GxrqC695LjEMAJMq9jFjCgVic5DEQhELJc94txquE1os1MPJXaiTn5OKpUItMTH8VfmVXBtb1'
  managehome => true,
}

ssh_authorized_key { 'my_only_key':
  ensure => 'present',
  key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC7375FoStRA+jl/Tf5f3BKZxgNpXFvTveAEdkRfoLOnxrKpD1qHNwG/s1owCo9NmmkSjc+uuVFvwg8k8EZy8/G+sUxiGM8PobrlT9KzbULMmQWNh+oyRvLY9ZP4t0fp+N6nUmvzrZeJHXgBNlV9hxlzfDK/v5GYSR1mQ8fx8blGeRV1fBcDAl54HQCSxSPk5u28YI7AuJWE1/36Ukj7JeCFZxaOeQaW+lXhj2W6+g6UPwQfTj1nlxQxDQD481q+B7rCJqN3syWfRI5g3+EEe9z55lQ9Dhkg74oQUbMr7HLNCJ23rMrIxHnoV2bD16YgePSwaDnX+Vcj5kvwT+NffXr',
  type   => 'ssh-rsa',
  user   => 'dlwillson',
}

file { '/etc/sudoers.d/dlwillson':
  ensure => 'present',
  content => 'dlwillson ALL=(ALL) NOPASSWD:ALL',
}

}
