class profiles::admins::dlwillson {

user { 'dlwillson':
  ensure     => present,
  password   => '$6$IiUTBT7S$GATFwsA42DHGhs7YRFdcQWqFkqEByl75HVTFHKUagFgImb1do.8onpD7AuHUypXCwMsku1YolIR3hsKNpVYpJ.',
  managehome => true,
}

ssh_authorized_key { 'dlwillson@dlwillson-P370EM':
  ensure => 'present',
  key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDxMrEBdREJ3UlWZFAcUTeJVl1m0fJ/xzPKusrW7V/z3CqIIDDQaaNm6aDg71QFJCxU/Vjyhi66xN3f9xlKsJdlUY26HgoMRHOVcSd6r1Kwdp/JH6+r45QjDh0ltSS/bPcdZVK0kYKZ/VJnmaczv4+4esroRHa7U2PfY5FqoCmAZfjF8DOPNHDfLAFfunAwQmEL9bHEIm6Ip7ptk124kJTC2sJSZqtoLErFg3+BiZVC+zPv0teFXNml68zXhN/5CAiQXhoauctxaCmTNhzEWuBF7xEBT1eYvPcB6HgVWnE75F60WndHQcxuEMvtxCwvRydjKH9QBiD7VHtFFYChz3O9`',
  type   => 'ssh-rsa',
  user   => 'dlwillson',
}

# Without SAZ's sudo module, use this file type resource.
# file { '/etc/sudoers.d/dlwillson':
#   ensure  => 'present',
#   content => 'dlwillson ALL=(ALL) NOPASSWD:ALL',
# }

# With SAZ's sudo module, you can use a sudo::conf defined type resource
# ( which syntax checks )
sudo::conf { 'dlwillson':
  content  => 'dlwillson ALL=(ALL) NOPASSWD: ALL',
}

}
